# Pagination Beaumarchais


![pagination_beaumarchais](./prive/themes/spip/images/pagination_beaumarchais-xx.svg)

Ce plugin propose un modèle de pagination qui s'adapte aux longues listes :
- le pas est variable et correspond à environ 1/10 du nombre d'éléments de la liste, arrondi à la dizaine la plus proche au delà de 50
  - si on a 10 pages, on aura un lien toutes les 1 page
  - si on a 20 pages, on aura un lien toutes les 2 pages
  - si on a 30 pages, on aura un lien toutes les 3 pages
  - si on a 40 pages, on aura un lien toutes les 4 pages
  - si on a 50 pages, on aura un lien toutes les 5 pages
  - si on a 60 pages, on aura un lien toutes les 10 pages
  - si on a 100 pages, on aura un lien toutes les 10 pages
  - si on a 200 pages, on aura un lien toutes les 20 pages
  - si on a 300 pages, on aura un lien toutes les 30 pages
  - ...
- on affiche +/- 3 pages autour de la page courante
- la première et dernière page sont toujours accessibles, par leurs numéros de page

On va donc avoir au plus 
  - 11 liens pour les accès directs aux pages par 10aines
  - 7 liens pour accéder à la page autour de la page courante
On va donc avoir au moins 
  - 10 liens pour les accès directs aux pages par 10aines
  - 3 liens pour accéder à la page autour de la page courante

Donc entre 13 et 18 liens dans la pagination

```
<BOUCLE_articles(ARTICLES) {!par date} {pagination}>
    <div class="hentry clearfix">
        [(#LOGO_ARTICLE_RUBRIQUE{#URL_ARTICLE}|image_reduire{150,*})]
        <h3 class="h2 entry-title"><a href="#URL_ARTICLE" rel="bookmark">#TITRE</a></h3>
        <small><abbr class="published">[(#DATE|affdate_jourcourt)]</abbr>[, <:par_auteur:> (#LESAUTEURS|supprimer_tags)]</small>
        [<div class="#EDIT{intro} introduction entry-content">(#INTRODUCTION)</div>]
    </div>
</BOUCLE_articles>
[<nav role="navigation" class="p pagination">(#PAGINATION{beaumarchais})</nav>]
</B_articles>
```


Il est possible de le personnaliser au cas par cas via

```
[<nav role="navigation" class="p pagination">(#PAGINATION{beaumarchais,label_plus="Voir plus d'articles…"})</nav>]
```
