<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}



function filtre_pages_pagination_beaumarchais_dist($page_courante, $nombres_pages) {
	$pas_page = intval(round($nombres_pages / 10));
	// si le pas de page est > 10 on arrondit au pas de page multiple de 10 le plus proche
	if ($pas_page > 5) {
		$pas_page = intval(round($pas_page / 10) * 10);
	}
	$pages = range($pas_page, $nombres_pages, $pas_page);
	for ($i = $page_courante - 3; $i <= $page_courante +3; $i++) {
		$pages[] = min(max($i, 1), $nombres_pages);
	}
	$pages[] = 1;
	$pages[] = $nombres_pages;
	sort($pages);
	$pages = array_unique($pages);
	$pages = array_map('intval', $pages);
	return $pages;
}
